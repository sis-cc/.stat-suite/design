$(document).ready(function () {

	$("#select2").hide();
	$("#select1").change(function(){
		if($(this).val() == 1){
			$("#select2").show();
		}else{
			$("#select2").hide();
		}
	});

    // form validator
    $('#contact-form').validator()


    // shows the top button in the right bottom corner when needed
    $("#top").hide();
    $(window).scroll(function () {
        if ($(this).scrollTop() > 140) {
            $("#top").fadeIn("slow");
        } else {
            $("#top").fadeOut("slow");
        }
    });

    // scrolls to the top of the page
    $("a[href=#top], #top").click(function () {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });

    // owl carousel
    $("#logo-scroller").owlCarousel({
        autoPlay: 3000, //Set AutoPlay to 3 seconds
        items: 5,
        itemsDesktop: [1199, 5],
        itemsDesktopSmall: [979, 3]
    });
    $("#logo-scroller2").owlCarousel({
        autoPlay: 3000, //Set AutoPlay to 3 seconds
        items: 2,
        itemsDesktop: [1199, 2],
        itemsDesktopSmall: [979, 2]
    });


    $('.rev-slider-fixed,.rev-slider-full').css('visibility', 'visible');

    /* Full */
    $('.rev-slider-banner-full').revolution({
        delay: 9000,
        startwidth: 1170,
        startheight: 500,
        onHoverStop: "on",
        thumbWidth: 100,
        thumbHeight: 50,
        thumbAmount: 3,
        hideThumbs: 0,
        navigationType: "none",
        navigationArrows: "solo",
        navigationStyle: "bullets",
        navigationHAlign: "center",
        navigationVAlign: "bottom",
        navigationHOffset: 30,
        navigationVOffset: 30,
        soloArrowLeftHalign: "left",
        soloArrowLeftValign: "center",
        soloArrowLeftHOffset: 20,
        soloArrowLeftVOffset: 0,
        soloArrowRightHalign: "right",
        soloArrowRightValign: "center",
        soloArrowRightHOffset: 20,
        soloArrowRightVOffset: 0,
        touchenabled: "on",
        stopAtSlide: -1,
        stopAfterLoops: -1,
        hideCaptionAtLimit: 0,
        hideAllCaptionAtLilmit: 0,
        hideSliderAtLimit: 0,
        fullWidth: "on",
        fullScreen: "off",
        fullScreenOffsetContainer: "#topheader-to-offset",
        shadow: 0

    });
});
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'line'
        },
        credits: {
            enabled: false
        },
        title: {
            text: 'My custom title',
            style: { "display": "none" }
        },
        legend: {
            enabled: false
        },
        xAxis: {
            title: {
                text: 'time'
            },
            labels: {
                enabled: false
            },
            plotLines: [{
                color: '#ddd',
                width: 1,
                value: 0
            }]
            //categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },

        series: [{
            data: [-300, -240, 0, 5.9, 7.4, 178.2, 88.0, 197.6, 248.5, 750.6, 1565.4]
        }],
        yAxis: {
            labels: {
                enabled: false
            },
            title: {
                text: 'open, innovative and industrialised'
            },
            gridLineWidth: 0
        }
    });
});