function scroll_to(clicked_link, nav_height) {
	var element_class = clicked_link.attr('href').replace('#', '.');
	var scroll_to = 0;
	if(element_class != '.top-content') {
		element_class += '-container';
		scroll_to = $(element_class).offset().top - 56;
	}
	if($(window).scrollTop() != scroll_to) {
		$('html, body').stop().animate({scrollTop: scroll_to}, 1000);
	}
}

$(document).ready(function () {
	$('a.scroll-link').on('click', function(e) {
		e.preventDefault();
		scroll_to($(this), $('nav').outerHeight());
	});
	
	$("#top").hide();
	$(window).scroll(function () {
		if ($(this).scrollTop() > 140) { // shows the top button in the right bottom corner when needed
			$("#top").fadeIn("slow");
		} else {
			$("#top").fadeOut("slow");
		}
	});
	
	
	
	var e = $('.carcard');
	for (var i = 0; i < 7; i++) {
		e.clone().insertAfter(e);
	}
	
	/*$('table.responsive').ngResponsiveTables({
		smallPaddingCharNo: 13,
		mediumPaddingCharNo: 18,
		largePaddingCharNo: 30
	});*/
});