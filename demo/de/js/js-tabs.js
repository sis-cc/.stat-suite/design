$(function() {
	$( "#slider-range" ).slider({
		range: true,
		min: 2001,
		max: 2015,
		values: [ 2003, 2013 ],
		slide: function( event, ui ) {
			$( "#amount1" ).text( ui.values[ 0 ]);
			$( "#amount2" ).text( ui.values[ 1 ]);
		}
	});
	$( "#amount1" ).text($( "#slider-range" ).slider( "values", 0 ));
	$( "#amount2" ).text($( "#slider-range" ).slider( "values", 1 ));
	
	$( "#slider-range2" ).slider({
		range: true,
		min: 2001,
		max: 2015,
		values: [ 2003, 2013 ],
		slide: function( event, ui ) {
			$( "#amount3" ).text( ui.values[ 0 ]);
			$( "#amount4" ).text( ui.values[ 1 ]);
		}
	});
	$( "#amount3" ).text($( "#slider-range2" ).slider( "values", 0 ));
	$( "#amount4" ).text($( "#slider-range2" ).slider( "values", 1 ));
});
$(document).ready(function() {
	$('.dimensions li').click(function(){
		$('.dimensions li.active').removeClass('active');
		$(this).not('.inactive').addClass('active');
		return false;
	});
	
	$('.chart-type a').click(function(){
		$('.chart-type a.active').removeClass('active');
		$(this).not('.inactive').addClass('active');
		return false;
	});
	
	$('.dimensions li a').click(function(){
		if ($('.edit-area').is(':visible')) {
		
			// do nothing
			
		} else {
			$('.edit-area').slideUp('slow');
			$('.edit-area').slideToggle('slow');
		}
	});
	
	$('.inactive').click(function(){
		return false;
	});
	
});