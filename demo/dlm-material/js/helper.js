$(document).ready(function () {
	$(".collapse-header").click(function () {
		$header = $(this);
		$collapse = $header.next();
		$collapse.slideToggle(500);
		if($(this).children('i').hasClass('material-expand_less')) {
			$(this).children('.collapse-header i').removeClass('material-expand_less');
			$(this).children('.collapse-header i').addClass('material-expand_more');
		} else {
			$(this).children('.collapse-header i').addClass('material-expand_less');
			$(this).children('.collapse-header i').removeClass('material-expand_more');
		}
	});
	
	$(".hidethis").hide();
	$(".updated").hide();
	
	$(".collapse-btn").click(function () {
		$(this).parents(".structure-row").find(".hidethis").slideToggle(500);
		$(this).parents(".structure-row").find(".updated").slideToggle(500);
	});
	
});