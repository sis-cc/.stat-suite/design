$(document).ready(function () {
	//$.fn.hummingbird.defaults.checkDoubles= true;
	//$.fn.hummingbird.defaults.checkboxes= "disabled";
	
	$("#treeview").hummingbird();
	$("#treeview2").hummingbird();
	
	$("#treeview").on("CheckUncheckDone", function(){
		var List = {"id" : [], "dataid" : [], "text" : []};
		$("#treeview").hummingbird("getChecked",{list:List,onlyEndNodes:true});
		$("#displayItems").html(List.text.join("<br>"));
		var L = List.id.length;
		if (L == 1) {
			$(".num").text(L);
		} else {
			$(".num").text(L);
		}
		var K = List.text;
		// alert(K);
		
		$("input[type=checkbox]:checked").next("span").parent("label").addClass("pt-active-1").removeClass("pt-active-2");
		$("input[type=checkbox]:indeterminate").next("span").parent("label").addClass("pt-active-2").removeClass("pt-active-1");
		
		//$("input[type=checkbox]").is(":indeterminate").next("span").addClass("pt-active2").removeClass("pt-active");
		//$("input[type=checkbox]").is(":checked").next("span").removeClass("pt-active2").addClass("pt-active");
		
		var str = "";

		$('input.hummingbird-end-node[type=checkbox]:checked').each(function(){
		  str += "<span class='pt-tag pt-tag-removable pt-minimal pt-intent-primary'>" + $(this).next("span").text() + "<button class='pt-tag-remove'></button></span>&nbsp;";
		})

		$('#mysidebarID').html(str);
		
		$("#treeview span:contains('(')").html(function () {
			return $(this).html().replace(" (", " <small class='pt-text-muted'>("); 
		});
		$("#treeview span:contains(')')").html(function () {
			return $(this).html().replace(")", ")</small>"); 
		});
		
	});
	
	$("#hum_41").next("span").parent("label").addClass("default");
	
	$("#treeview").on("nodeUnchecked", function(){
		$("input[type=checkbox]").next("span").parent("label").removeClass("pt-active-1");
		$("input[type=checkbox]").next("span").parent("label").removeClass("pt-active-2");
	});
	
	
	 
	 $("#treeview").hummingbird("checkNode",{attr:"id",name: "hum_41",expandParents:false});
	 $("#treeview").hummingbird("disableNode",{attr:"id",name: "hum_41",state:true,disableChildren:false});
	 $("#treeview").hummingbird("expandNode",{attr:"id",name: "hum_41",expandParents:true});
	 
});